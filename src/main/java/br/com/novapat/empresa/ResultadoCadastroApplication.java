package br.com.novapat.empresa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ResultadoCadastroApplication {

    public static void main(String[] args) {
        SpringApplication.run(ResultadoCadastroApplication.class, args);
    }

}

