package br.com.novapat.empresa.cadastro;

import br.com.novapat.empresa.models.ResultadoValidacaoEmpresa;
import br.com.novapat.empresa.service.ResultadoArquivoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class ResultadoValidacaoListener {

    @Autowired
    private ResultadoArquivoService resultadoArquivoService;

    @KafkaListener(topics = "spec3-patricia-novaes-3", groupId = "grupo1")
    public void registraResultado(@Payload ResultadoValidacaoEmpresa resultado) {
        resultadoArquivoService.registrar(resultado);
    }
}
