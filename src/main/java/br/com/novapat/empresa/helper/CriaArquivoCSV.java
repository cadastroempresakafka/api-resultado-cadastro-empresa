package br.com.novapat.empresa.helper;

import br.com.novapat.empresa.models.ResultadoValidacaoEmpresa;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class CriaArquivoCSV {

    public static final String ACEITOS_CSV = "aceitos.csv";
    public static final String NEGADOS_CSV = "negados.csv";

    public void registraAcesso(ResultadoValidacaoEmpresa empresa) {
        if (empresa.isCapitalAceito()) {
            registraEmpresaLiberada(empresa);
        }
        else {
            registraEmpresaNegada(empresa);
        }
    }

    private void registraEmpresaLiberada(ResultadoValidacaoEmpresa empresa) {
        escreveArquivoEmpresas(ACEITOS_CSV, empresa);
    }

    private void registraEmpresaNegada(ResultadoValidacaoEmpresa empresa) {
        escreveArquivoEmpresas(NEGADOS_CSV, empresa);
    }

    private void escreveArquivoEmpresas(String arquivo, ResultadoValidacaoEmpresa empresa) {
        try {
            String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy-hh:mm:ss"));
            String registro = String.format("%s,%s,%s\n", empresa.getNome(), empresa.getCnpj(), empresa.getCapitalSocial() ,timestamp);
            Files.write(Paths.get(arquivo), registro.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
