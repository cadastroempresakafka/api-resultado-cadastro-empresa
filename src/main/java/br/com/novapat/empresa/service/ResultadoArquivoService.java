package br.com.novapat.empresa.service;

import br.com.novapat.empresa.helper.CriaArquivoCSV;
import br.com.novapat.empresa.models.ResultadoValidacaoEmpresa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResultadoArquivoService {

    @Autowired
    CriaArquivoCSV criaArquivoCSV;

    public void registrar(ResultadoValidacaoEmpresa resultado) {
        System.out.printf("CNPJ: %s - %s\n", resultado.getCnpj(), resultado.isCapitalAceito() ? "ACEITO" : "NEGADO");
        criaArquivoCSV.registraAcesso(resultado);
    }
}
